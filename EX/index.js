// EX1: Xuất ba số theo thứ tự tăng dần
// const document.getElementById("result").innerHTML = document.getElementById("result").innerHTML;

function sapXep() {
  var soThu1 = document.getElementById("so_thu_1").value;

  var soThu2 = document.getElementById("so_thu_2").value;

  var soThu3 = document.getElementById("so_thu_3").value;

  var a = soThu1;
  var b = soThu2;
  var c = soThu3;
  if (a < b) {
    if (a < c) {
      if (b < c) {
        document.getElementById("result").innerHTML = a + "," + b + "," + c;
      } else {
        document.getElementById("result").innerHTML = a + "," + c + "," + b;
      }
    } else {
      document.getElementById("result").innerHTML = c + "," + a + "," + b;
    }
  } else {
    if (b < c) {
      if (a < c) {
        document.getElementById("result").innerHTML = b + "," + a + "," + c;
      } else {
        document.getElementById("result").innerHTML = b + "," + c + "," + a;
      }
    } else {
      document.getElementById("result").innerHTML = c + "," + b + "," + a;
    }
  }
}

// EX: chương trình chào hỏi

function guiLoiChao() {
  var option = document.getElementById("selection").value;

  if (option == "b") {
    document.getElementById("guiLoiChao").innerHTML = "Xin chào Bố!";
  } else if (option == "m") {
    document.getElementById("guiLoiChao").innerHTML = "Xin chào Mẹ!";
  } else if (option == "a") {
    document.getElementById("guiLoiChao").innerHTML = "Xin chào Anh trai!";
  } else {
    document.getElementById("guiLoiChao").innerHTML = "Xin chào Em gái!";
  }
}

// EX3:Đếm số chẵn lẻ
function Dem() {
  var number1 = document.getElementById("number1").value * 1;

  var number2 = document.getElementById("number2").value * 1;

  var number3 = document.getElementById("number3").value * 1;

  var tongSoChan = 0;
  var tongSoLe = 0;

  if (number1 % 2 == 0) {
    tongSoChan++;
  } else {
    tongSoLe++;
  }
  if (number2 % 2 == 0) {
    tongSoChan++;
  } else {
    tongSoLe++;
  }
  if (number3 % 2 == 0) {
    tongSoChan++;
  } else {
    tongSoLe++;
  }

  document.getElementById(
    "Dem"
  ).innerHTML = `Có ${tongSoChan} số chẵn, có ${tongSoLe} số lẻ`;
}

// EX4: Đoán hình tam giác
function xacDinh() {
  var canh1 = document.getElementById("canh1").value * 1;

  var canh2 = document.getElementById("canh2").value * 1;

  var canh3 = document.getElementById("canh3").value * 1;

  var a = canh1;
  var b = canh2;
  var c = canh3;

  if (a + b > c && a + c > b && b + c > a) {
    if (a == b && a == c && b == c) {
      document.getElementById("xacDinh").innerHTML = "Hình tam giác đều";
    } else if (a == b || b == c || a == c) {
      if (
        Math.pow(c, 2) == Math.pow(a, 2) + Math.pow(b, 2) ||
        Math.pow(b, 2) == Math.pow(a, 2) + Math.pow(c, 2) ||
        Math.pow(a, 2) == Math.pow(b, 2) + Math.pow(c, 2)
      ) {
        document.getElementById("xacDinh").innerHTML =
          "Hình tam giác vuông cân";
      } else {
        document.getElementById("xacDinh").innerHTML = "Hình tam giác cân";
      }
    } else if (
      Math.pow(c, 2) == Math.pow(a, 2) + Math.pow(b, 2) ||
      Math.pow(b, 2) == Math.pow(a, 2) + Math.pow(c, 2) ||
      Math.pow(a, 2) == Math.pow(b, 2) + Math.pow(c, 2)
    ) {
      document.getElementById("xacDinh").innerHTML = "Hình tam giác vuông";
    } else {
      document.getElementById("xacDinh").innerHTML = "Một loại tam giác khác";
    }
  } else {
    alert("Dữ liệu không hợp lệ");
  }
}
