if (a + b > c && a + c > b && b + c > a) {
  if (a == b && a == c && b == c) {
    document.getElementById("xacDinh").innerHTML = "Hình tam giác đều";
  } else if (a == b || b == c || a == c) {
    document.getElementById("xacDinh").innerHTML = "Hình tam giác cân";
  } else if (
    Math.pow(c, 2) == Math.pow(a, 2) + Math.pow(b, 2) ||
    Math.pow(b, 2) == Math.pow(a, 2) + Math.pow(c, 2) ||
    Math.pow(a, 2) == Math.pow(b, 2) + Math.pow(c, 2)
  ) {
    document.getElementById("xacDinh").innerHTML = "Hình tam giác vuông";
  } else {
    document.getElementById("xacDinh").innerHTML = "Một loại tam giác khác";
  }
} else {
  alert("Dữ liệu không hợp lệ");
}
